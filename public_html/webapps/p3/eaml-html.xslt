<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html" />
   <!--ROOT ELEMENT-->
   <xsl:template match="Degree">
      <html>
      <table border="1">
         <tr><th colspan="4" scope="rowgroup"><b>
            Grao:
            <xsl:value-of select="Name" />
            (scope:
            <xsl:value-of select="Scope" />
            , campus:
            <xsl:value-of select="./@location" />
            )
         </b></th></tr>
         <xsl:apply-templates select="Course" />
      </table>
      <br/>
      <div>
         <button onClick="location.href='?p=MAT30KRACK&amp;pphase=01'" type="button"> Inicio </button>
         <button onClick="location.href='?p=MAT30KRACK&amp;pphase=21'" type="button"> Atrás </button>
         <hr/>
         <p> Mateo G. Cid (2020/21) </p>
      </div>
   </html>
   </xsl:template>
   <!--COURSE-->
   <xsl:template match="Degree/Course">
      <xsl:for-each select="current()">
         <tr><th colspan="4" scope="rowgroup"><b>
            Curso
            <xsl:value-of select="./@number" />
         </b></th></tr>
         <xsl:apply-templates select="Subject" />
      </xsl:for-each>
   </xsl:template>
   <!--SUBJECTS & STUDENTS-->
   <xsl:template match="Degree/Course/Subject">
      <xsl:for-each select="current()">
            <tr>
               <th colspan="4" scope="rowgroup"><xsl:value-of select="Name"/>
               (id: <xsl:value-of select="./@idSub"/>, tipo: <xsl:value-of select="./@type"/>)
               </th>
            </tr>
            <tr>
               <th>Alumnx</th>
               <th>Direccion</th>
               <th>ID</th>
               <th>Nota</th>
            </tr>
            <xsl:for-each select="Student">
               <tr>
                  <td>
                     <xsl:value-of select="Name" />
                  </td>
                  <td>
                     <xsl:for-each select="text()">
                        <xsl:value-of select="current()"/>
                     </xsl:for-each>
                  </td>
                  <td>
                     <xsl:choose>
                        <xsl:when test="Resident != ''">
                           <xsl:value-of select="Resident" />
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="Dni" />
                        </xsl:otherwise>
                     </xsl:choose>
                  </td>
                  <td>
                     <xsl:value-of select="Grade" />
                  </td>
               </tr>
            </xsl:for-each>
      </xsl:for-each>
   </xsl:template>
</xsl:stylesheet>

