<!--PHP PAGE-->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Los Gatos</title>
    <!--FAVICON!-->
    <link rel="icon" href="favicon.ico">
    <!--CSS, JS, PHP-->
    <link rel="stylesheet" href="p1.css">
    <!--scripts-->
    <script defer src="p1.js"></script>
    <!--browser version-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bowser/1.9.4/bowser.min.js"></script>
</head>

<body>
    <div id=menu_div>
        <header id=menu>
            <ul id=menu_list>
                <li id=web_logo>
                    <img id=logo_img src="assets/tenor.gif" width="90" />
                </li>
                <li id=gatetes>
                    <a href="#">
                        Gatetes
                    </a>
                </li>
                <li>
                    <a href='#'>
                        Taguegues
                    </a>
                </li>
            </ul>
        </header>
    </div>
    <main>
        <div id=forms_div>
            <section class ="parapara">
                <h2>Los Gatos: la respuesta</h2>
                <h3>Para el profe</h3>
                <script>
                    document.write(bowser.name, "-", bowser.version);
                </script>
                <ul class=answers_list>
                    <!-- https://www.php.net/manual/en/reserved.variables.server.php -->
                    <li><b>Server name: </b><?php echo $_SERVER['SERVER_NAME']?>
                    <li><b>Server port: </b><?php echo $_SERVER['SERVER_PORT']?>
                    <li><b>Script name: </b><?php echo $_SERVER['SCRIPT_NAME']?>
                    <br>
                </ul>
                <h3>Para mi</h3>
                <ul class=answers_list>
                    <li>
                        <?php 
                         if (isset($_POST['submit'])){
                            $avatar = $_FILES['avatar'];
                            $avatar_name=$avatar['name'];
                            $avatar_tmp=$avatar['tmp_name'];
                            $avatar_size=$avatar['size'];
                            $avatar_error=$avatar['error'];

                            // play with the xtension
                            $avatar_ext = explode('.', $avatar_name);
                            $avatar_extension=strtolower(end($avatar_ext));
                            
                            $allow = array('jpeg','jpg', 'png', 'gif');
                            if (in_array($avatar_extension,$allow)) {
                                if ($avatar_error === 0){
                                    if ($avatar_size < 50000000){ //50 Mbi
                                        // to avoid repeated ids
                                        $avatar_neoname = uniqid('','true').".".$avatar_extension;
                                        // store in uploads folder
                                        $avatar_store ='uploads/'.$avatar_neoname;
                                        move_uploaded_file($avatar_tmp, $avatar_store);
                                    }
                                    else{print("imagen demasiado grande");} 
                                }
                                else{print("error: no se puede mostrar la imagen");}
                            }
                            else{print("solo valen png, jpeg, jpg y gif!");}                              
                        }
                         else{
                            print("no se pueden mostrar imágenes con GET");
                        }
                        ?>
                        <img src=<?php echo $avatar_store?> width="150" alt="">
                    </li>
                    <?php
                        if (isset($_POST['submit'])){
                            $username = $_POST['username'];
                            $password = $_POST['password'];
                            $email = $_POST['email'];
                            $telephone = $_POST['telephone'];
                            $date = $_POST['date'];
                        }                        
                        elseif (isset($_GET['submit'])){
                            $username = $_GET['username'];
                            $password = $_GET['password'];
                            $email = $_GET['email'];
                            $telephone = $_GET['telephone'];
                            $date = $_GET['date'];
                        }
                     ?>
                    <li><b>Username:</b> <?php echo $username;?> </li>
                    <li><b>Password:</b> <?php echo $password;?></li>
                    <li><b>Email:</b> <?php echo $email;?></li>
                    <li><b>Teléfono:</b> <?php echo $telephone;?></li>
                    <li><b>Fecha:</b> <?php echo $date;?></li>
                </ul>
                <h3>Para los gatos</h3>
                <ul class=answers_list>
                    <li><b>Comilonas:</b>
                    <?php 
                        if (isset($_POST['submit'])){
                            echo $_POST['comida_gato'];
                        }
                        elseif (isset($_GET['submit'])){
                            echo $_GET['comida_gato'];
                        }
                        ?></li>
                    <li><b>Peliculeres:</b>
                    <?php
                        if (isset($_POST['submit'])){
                            if($_POST['peli_garfield']=='1'){echo " garfield -";}
                            if($_POST['peli_ali']=='1'){echo " aliG -";}
                            if($_POST['peli_gato']=='1'){echo " gato con Sombrero -";}
                            if($_POST['peli_malv']=='1'){echo " malviviendo -";}
                        }
                        elseif (isset($_GET['submit'])){
                            if($_GET['peli_garfield']=='1'){echo " garfield -";}
                            if($_GET['peli_ali']=='1'){echo " aliG -";}
                            if($_GET['peli_gato']=='1'){echo " gato con Sombrero -";}
                            if($_GET['peli_malv']=='1'){echo " malviviendo -";}
                        }
                        /*
                        if (isset($_POST['submit'])){
                        }
                        elseif (isset($_GET['submit'])){
                        }
                        */
                        ?>
                    </li>
                </ul>
                    
            </section>
            
        </div>
    </main>
</body>
</html>