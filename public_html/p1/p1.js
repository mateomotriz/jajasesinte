// FORM
var form = document.getElementById('form_register')

// inputs
var username = document.getElementById('input_username')
var password = document.getElementById('input_passwd')
var email = document.getElementById('input_email')
var telephone = document.getElementById("input_tel")
var date = document.getElementById("input_date")

// selectable
var radio = document.getElementsByClassName('radio_gato')
var check = document.getElementsByClassName('check_gato')
var radiogetpost = document.getElementsByClassName('check_getpost');
var radioenctype = document.getElementsByClassName('check_enctype');

// formats
var mail_format = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/;

// errors
//var error_text = document.getElementById('error_user')
var error_text = document.getElementsByClassName('error_message')
var error_bool = false;

// values
var value_username, value_password, value_email, value_telephone, value_date, value_radio;
var value_check;

// PHP INFO button
var info_button = document.getElementById('info_button');
var submit_button = document.getElementById('submit_button');

form.addEventListener('submit', (e) => {
    error_bool = false;

    // username
    /*
    if (username.value === '' || username.value === null) {
        error_text[0].innerText = "Elige un username guapo!";
    } else {
        value_username = username.value;
        error_text[0].innerText = null;
    }
    */

    // passwd
    if (password.value === '' || password.value === null) {
        error_text[1].innerText = null;
    } else if (password.value.length <= 6) {
        error_bool = true;
        error_text[1].innerText = "Debe tener más de 6 caracteres";
    } else {
        value_password = password.value;
        error_text[1].innerText = null;
    }

    // email
    if (email.value === '' || email.value === null) {
        error_text[2].innerText = null;
    } else if (!mail_format.test(email.value)) {
        error_bool = true;
        error_text[2].innerText = "ej: ma@teo.com";
    } else {
        value_email = email.value;
        error_text[2].innerText = null;
    }


    // telephone
    if (telephone.value === '' || telephone.value === null) {
        error_text[3].innerText = null;
    } else if (isNaN(telephone.value) || telephone.value.length != 9) {
        error_bool = true;
        error_text[3].innerText = "9 números y sin espacios";
    } else {
        value_telephone = telephone.value;
        error_text[3].innerText = null;
    }


    //date
    value_date = date.value;
    console.log("eoe");
    console.log("date: " + value_date);

    // radio
    for (i = 0; i < radio.length; i++) {
        //console.log(radio[i].value + " & " + radio[i].checked);
        if (radio[i].checked === true) {
            value_radio = radio[i].value;
        }
    }

    // checkbox
    for (i = 0; i < check.length - 2; i++) {
        //console.log(check[i].value + " & " + check[i].checked);
        if (check[i].value === true) {
            value_check = check[i].value;
        }
    }
    // mark all
    if (check[check.length - 2].value === true) {
        for (i = 0; i < check.length - 2; i++) {
            check[i].checked = true;
        }
    }
    // unmark all
    if (check[check.length - 1].value === true) {
        for (i = 0; i < check.length - 2; i++) {
            check[i].checked = false;
        }
    }

    /* ENCTYPE & GET/POST */
    // options: "multipart/form-data, text/plain"
    //form.enctype = "multipart/form-data";
    // options: "GET, POST"
    //form.method = "POST";

    if (radiogetpost[0].checked === true) {
        form.method = "GET";
        console.log("get");
    } else if (radiogetpost[1].checked === true) {
        form.method = "POST";
        console.log("post");
    }
    console.log("form: " + form.method);

    if (radioenctype[0].checked === true) {
        form.enctype = "multipart/form-data";
    } else if (radioenctype[1].checked === true) {
        form.enctype = "text/plain";
    }
    console.log("enctype: " + form.enctype);

    if (error_bool === true) {
        // avoid submitting
        //alert("soy a prueba de balas")
        e.preventDefault();
    } else {}

    // navigator
    var nv = "Browser: " + String(bowser.name) + "/" + String(bowser.version)
    navigator_v.innerText = nv;
})

// mark/unmark
check[check.length - 2].addEventListener('click', (e) => {
    e.preventDefault();
    //mark
    for (i = 0; i < check.length - 2; i++) {
        check[i].checked = true;
    }
    check[check.length - 1].checked = false;
    return false;
})

check[check.length - 1].addEventListener('click', (e) => {
    e.preventDefault();
    //unmark
    for (i = 0; i < check.length - 2; i++) {
        check[i].checked = false;
    }
    check[check.length - 2].checked = false;
    return false;
})

info_button.addEventListener('click', (e) => {
    form.action = "phpinfo.php";
    console.log("php info: ", form.action);
})

submit_button.addEventListener('click', (e) => {
    form.action = "p1.php";
    console.log("php info: ", form.action);
})


// navigator version
var navigator_v = document.getElementById("navigator_v")

document.onload = (() => {
    navigator_v.innerText(bowser.name, bowser.version);
    console.log("browser: ", bowser.name, bowser.version);
})