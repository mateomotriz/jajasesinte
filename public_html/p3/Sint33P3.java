package p3;

// Mateo G. Cid :)
// UTILS
import java.util.*;

// SERVLET
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// XPATH
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;

// XSLT
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

// OTHERS
import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class Sint33P3 extends HttpServlet {
      // DATA 2 RETRIEVE
      private final String SUPER_SECRET_PASSWORD = "MAT30KRACK";

      // DEGREE'S DATA
      HashMap<String, Document> mapDocs = new HashMap<>();
      HashMap<String, String> mapFiles = new HashMap<>();
      ArrayList<String> unreadDocs = new ArrayList<>();
      ArrayList<String> filesList = new ArrayList<String>();
      ArrayList<ErrorFile> errorsList = new ArrayList<ErrorFile>();
      ArrayList<Subject> subjectsList = new ArrayList<>();
      ArrayList<Student> studentsList = new ArrayList<>();

      // ERROR CODES
      private final String ERROR_NOPASSWORD = "NO PASSWD";
      private final String ERROR_WRONGPASSWD = "WRONG PASSWD";
      private final String ERROR_PARAMETERS = "MISSING PARAMETERS";
      private final String NO_ERROR = "OK";

      // SCHEMA
      final String INIT_FILE = "http://gssi.det.uvigo.es/users/agil/public_html/SINT/20-21/teleco.xml";
      // final String INIT_FILE = "http://localhost:7033/sint33/p3/teleco.xml";
      final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
      final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
      final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

      // XPATH
      XPathFactory xpathFactory;
      XPath xpath;

      // SAX
      SAXParser sp;
      SaxParserXML saxHandler;

      // STATE MACHINE
      enum FileState {
            INIT, VALIDATE, SAVE, LOOKFOR, ERROR_HANDLER, FINISH
      }

      /* MAIN */
      @Override
      public void init() throws ServletException {
            // XML
            try {
                  xmlHandler();
            } catch (ParserConfigurationException pce) {
                  System.out.println("ParserConfigurationException: " + pce.getMessage());
            } catch (SAXException saxe) {
                  System.out.println("SAXException: " + saxe.getMessage());
            }

            super.init();
      }

      /* SERVLET */
      @Override
      public void doGet(HttpServletRequest request, HttpServletResponse response) {
            // GET SYSTEM PARAMETERS
            getParametersURL(request, response);
      }

      /* METHODS ************************** :) */

      /* PARAMETERS FROM URL */
      private void getParametersURL(HttpServletRequest request, HttpServletResponse response) {
            // initially
            String auto = "", pphase = "", pdegree = "", psubject = "", passwd = "";
            String errorCode = "", missingParameter = "";
            errorCode = NO_ERROR;

            // aux
            boolean auxPasswd = true;

            try {
                  // check passwd
                  passwd = request.getParameter("p");
                  if (passwd == null) {
                        errorCode = ERROR_NOPASSWORD;
                        passwd = "";
                        auxPasswd = false;
                  } else if ((!passwd.equals(SUPER_SECRET_PASSWORD))) {
                        errorCode = ERROR_WRONGPASSWD;
                        auxPasswd = false;
                  }

                  // only if the passwd is correct!
                  if (auxPasswd) {
                        // auto mode
                        auto = request.getParameter("auto");
                        if (auto == null) {
                              auto = "false";
                        } else if (!auto.equals("true")) {
                              auto = "false";
                        }

                        // which phase
                        pphase = request.getParameter("pphase");
                        if (pphase == null) {
                              pphase = "01";
                        }

                        // which degree
                        pdegree = request.getParameter("pdegree");
                        if (pdegree == null) {
                              if (pphase.equals("12") || pphase.equals("13")) {
                                    missingParameter = "pdegree";
                                    errorCode = ERROR_PARAMETERS;
                              }
                        }

                        // which subject
                        psubject = request.getParameter("psubject");
                        if (psubject == null) {
                              if (pphase.equals("13")) {
                                    errorCode = ERROR_PARAMETERS;
                                    if (pdegree == null) {
                                          missingParameter = "pdegree";
                                    } else {
                                          missingParameter = "psubject";
                                    }
                              }
                        }
                  }

                  // INIT FRONT END
                  FrontEnd frontEnd = new FrontEnd(auto, passwd);

                  // INITIAL SETUP & PRINT
                  if (auto == "false") {
                        response.setContentType("text/html");
                  } else if (auto == "true") {
                        response.setContentType("text/xml");
                  }
                  response.setCharacterEncoding("UTF-8");
                  PrintWriter x = response.getWriter();

                  if (errorCode.equals(NO_ERROR)) {
                        switch (pphase) {
                              default:
                              case "01":
                                    frontEnd.printCase01(x);
                                    break;
                              case "02":
                                    frontEnd.printCase02(x, errorsList, countErrors("warning"), countErrors("error"),
                                                countErrors("fatalerror"));
                                    break;
                              case "11":
                                    frontEnd.printCase11(x, getC1Degrees(), "12");
                                    break;
                              case "12":
                                    frontEnd.printCase12(x, getC1Subjects(pdegree), pdegree);
                                    break;
                              case "13":
                                    frontEnd.printCase13(x, getC1Students(pdegree, psubject), pdegree, psubject);
                                    break;
                              // XSLT [P3]
                              case "21":
                                    frontEnd.printCase11(x, getC1Degrees(), "22");
                                    break;
                              case "22":
                                    x.println(parseXml2Html(pdegree));
                                    break;
                        }
                  } else {
                        switch (errorCode) {
                              // NO PASSWORD
                              case ERROR_NOPASSWORD:
                                    frontEnd.printWrongPasswd(x, false);
                                    break;
                              // WRONG PASSWD
                              case ERROR_WRONGPASSWD:
                                    frontEnd.printWrongPasswd(x, true);
                                    break;
                              // MISSING PARAMETERS
                              case ERROR_PARAMETERS:
                                    frontEnd.printMissingParameters(x, missingParameter);
                                    break;

                              default:
                                    break;
                        }
                  }
            }

            catch (Exception ex) {
                  System.out.println("Algo foi mal:" + ex.toString());
            }
      }

      /* XML PARSER */
      private void xmlHandler() throws ParserConfigurationException, SAXException {
            // AUX
            FileState fileState = FileState.INIT;
            boolean readFiles = false;

            // CONFIGURE SCHEMA
            String currentDoc = null;
            DocumentBuilder db = null;
            Document doc = null;
            DocumentBuilderFactory dbf;
            dbf = DocumentBuilderFactory.newInstance();
            dbf.setValidating(true);
            dbf.setNamespaceAware(true);

            // SCHEMA
            dbf.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            String pathSchema = getServletContext().getRealPath("/p3/eaml.xsd");
            File schemaFile = new File(pathSchema);
            dbf.setAttribute(JAXP_SCHEMA_SOURCE, schemaFile);

            // CONFIGURE ERROR HANDLER
            ErrorHandlerXML errorHandler = new ErrorHandlerXML();
            db = dbf.newDocumentBuilder();
            db.setErrorHandler(errorHandler);

            // XPATH
            xpathFactory = XPathFactory.newInstance();
            xpath = xpathFactory.newXPath();

            // SAX
            SAXParserFactory spf = SAXParserFactory.newInstance();
            sp = spf.newSAXParser();
            saxHandler = new SaxParserXML();

            // URL
            String[] absoluteURL;
            String fileURL = "";
            String prefixURL = "";

            while (!readFiles) {
                  switch (fileState) {
                        case INIT:
                              unreadDocs.clear();
                              unreadDocs.add(INIT_FILE);
                              fileState = FileState.VALIDATE;
                              break;
                        case VALIDATE:
                              // valid document?
                              currentDoc = unreadDocs.get(0);
                              try {
                                    doc = db.parse(currentDoc);

                                    // get URL (to fix the relative paths)
                                    absoluteURL = currentDoc.split("/");
                                    fileURL = absoluteURL[absoluteURL.length - 1];
                                    prefixURL = currentDoc.replace(fileURL, "");

                                    // check if there were any errors
                                    if (errorHandler.getHandlerError() != 0) {
                                          // get errors
                                          ErrorFile errorFile = new ErrorFile(currentDoc,
                                                      errorHandler.getErrorFeedbacks());

                                          errorsList.add(errorFile);
                                          fileState = FileState.FINISH;
                                    } else {
                                          // null doc?
                                          if (doc == null) {
                                                System.out.println("NULL: DOC - " + currentDoc);
                                                fileState = FileState.FINISH;
                                          } else {
                                                // System.out.println("OK: DOC - " + currentDoc);
                                                fileState = FileState.SAVE;
                                          }
                                    }
                              } catch (IOException ioe) {
                                    ErrorFeedback ioeMessage = new ErrorFeedback(ioe.getMessage().trim(), "fatalerror");
                                    errorsList.add(new ErrorFile(currentDoc, ioeMessage));
                                    fileState = FileState.FINISH;
                              } catch (SAXException se) {
                                    String auxSax = se.getMessage().trim();
                                    auxSax = auxSax.replace("\"<", "");
                                    auxSax = auxSax.replace(">\"", "");
                                    auxSax = auxSax.replace("/", "");
                                    ErrorFeedback seMessage = new ErrorFeedback(auxSax, "fatalerror");
                                    errorsList.add(new ErrorFile(currentDoc, seMessage));
                                    fileState = FileState.FINISH;
                              }

                              // mark document as read (doesnt matter if its not ok)
                              unreadDocs.remove(0);
                              break;
                        case SAVE:
                              // check if it already exists
                              try {
                                    // p2(xpath) - > p3(sax)
                                    saxHandler.setState("EAML");
                                    sp.parse(currentDoc, saxHandler);
                                    String rootDegree = saxHandler.getRootDegree();

                                    if (!mapDocs.containsKey(rootDegree)) {
                                          mapDocs.put(rootDegree, doc);
                                          mapFiles.put(rootDegree, currentDoc);
                                          fileState = FileState.LOOKFOR;
                                    } else {
                                          fileState = FileState.FINISH;
                                    }
                              } catch (SAXException saxe) {
                                    System.out.println("SAXException: " + saxe.getMessage());
                                    fileState = FileState.FINISH;
                              } catch (IOException ioe) {
                                    System.out.println("IOException: " + ioe.getMessage());
                                    fileState = FileState.FINISH;
                              }

                              break;
                        case LOOKFOR:
                              // look for more files
                              ArrayList<String> eamlList = new ArrayList<String>(saxHandler.getC1Eamls());
                              for (String eaml : eamlList) {
                                    // check link (abs/rel)
                                    if (!eaml.startsWith("http:/")) {
                                          eaml = prefixURL + eaml;
                                    }
                                    unreadDocs.add(eaml);
                              }
                              fileState = FileState.FINISH;
                        case FINISH:
                              if (unreadDocs.isEmpty()) {
                                    // sort ABC
                                    Collections.sort(errorsList, Comparator.comparing(ErrorFile::getDocName));

                                    // init servlet
                                    readFiles = true;
                              } else {
                                    fileState = FileState.VALIDATE;
                              }
                              break;
                        case ERROR_HANDLER:
                              break;
                        default:
                              break;
                  }
            }
      }

      /* GET THINGS! */
      private ArrayList<String> getC1Degrees() {
            filesList.clear();
            for (String degree : mapDocs.keySet()) {
                  filesList.add(degree);
            }

            // sort ABC
            Collections.sort(filesList, String.CASE_INSENSITIVE_ORDER);
            return filesList;
      }

      private ArrayList<Subject> getC1Subjects(String degree) {
            // SUBJECTS
            try {
                  String file = mapFiles.get(degree);
                  subjectsList.clear();

                  // parse w sax
                  saxHandler.setState("SUBJECT");
                  sp.parse(file, saxHandler);

                  // get array-list
                  subjectsList = new ArrayList<Subject>(saxHandler.getC1Subjects());

            } catch (Exception ex) {
                  System.out.println("exception: " + ex.getMessage());
            }

            // sort ABC
            Collections.sort(subjectsList, Comparator.comparing(Subject::getName));
            Collections.sort(subjectsList, Comparator.comparing(Subject::getCourse));
            return subjectsList;
      }

      private ArrayList<Student> getC1Students(String degree, String subject) {
            // STUDENTS
            try {
                  String file = mapFiles.get(degree);
                  studentsList.clear();

                  // parse w sax
                  saxHandler.setState("STUDENT");
                  saxHandler.setSubject(subject);
                  sp.parse(file, saxHandler);

                  // get array-list
                  studentsList = new ArrayList<Student>(saxHandler.getC1Students());

            } catch (Exception ex) {
                  System.out.println("exception: " + ex.getMessage());
            }

            // sort ABC
            Collections.sort(studentsList, Comparator.comparing(Student::getName));
            Collections.sort(studentsList, Comparator.comparing(Student::sortDni));

            return studentsList;
      }

      /* AUX */
      public int countErrors(String errorType) {
            int errorCounter = 0;
            ArrayList<String> errorTypeList = new ArrayList<String>();

            for (int i = 0; i < errorsList.size(); i++) {
                  // get errors
                  ArrayList<ErrorFeedback> efList = new ArrayList<ErrorFeedback>(errorsList.get(i).getErrorsList());
                  // check if there are w/e/fe
                  errorTypeList.clear();
                  for (int k = 0; k < efList.size(); k++) {
                        errorTypeList.add(efList.get(k).getType());
                  }
                  // show them if thats the case
                  if (errorTypeList.contains(errorType)) {
                        errorCounter++;
                  }
            }
            return errorCounter;
      }

      /* XTRA FUNCTIONALITY [P3] */
      private String parseXml2Html(String degree) {
            Document doc = mapDocs.get(degree);
            File output = new File("xslt.html");
            try {
                  // XSL FILE
                  String pathXSL = getServletContext().getRealPath("/p3/eaml-html.xslt");
                  File xslFile = new File(pathXSL);

                  // TRANSFORMATION
                  TransformerFactory factory = TransformerFactory.newInstance();
                  Source xslSrc = new StreamSource(xslFile);
                  Transformer transformer = factory.newTransformer(xslSrc);

                  DOMSource src = new DOMSource(doc);
                  transformer.transform(src, new StreamResult(output));

                  // FILE 2 STRING
                  String outStr = new String(Files.readAllBytes(output.toPath()), StandardCharsets.UTF_8);
                  return outStr;
            } catch (Exception ex) {
                  System.out.println("xslt problem: " + ex.toString());
                  return ex.toString();
            }
      }
}
