package p3;

import java.io.PrintWriter;
import java.util.*;

public class FrontEnd {
    private final String title = "MATEOS";
    String auto = "false";
    String passwd = "";

    // empty constructor
    public FrontEnd(String auto, String passwd) {
        this.auto = auto;
        this.passwd = passwd;
    }

    public void printCase01(PrintWriter x) {
        if (auto == "false") {
            // INIT HTML
            x.println("<!DOCTYPE html><html>");
            x.println("<head>");
            x.println("<meta charset=\"UTF-8\" />");
            x.println("<link rel=\"shortcut icon\" href=\"./favicon.ico\">");
            x.println("<title>" + title + "</title>");
            // x.println(<link rel="stylesheet" href="p1.css">)
            x.println("</head>");

            // BODY
            x.println("<body>");
            x.println("<main>");
            x.println("<h2>" + "Servizo de consulta de expedientes académicos" + "</h2>");
            x.println("</main>");

            // PPHASE
            x.println("<p> Benvido amigx </p>");
            x.println("<p> <a href=\"?p=" + passwd + "&pphase=02\"> Ver os ficheiros erróneos </a></p>");
            x.println("<p> Selecciona unha consulta: </p>");
            x.println("<li> <a href=\"?p=" + passwd
                    + "&pphase=11\"> Consulta 1a: ver xs alumnxs de unha asignatura de unha titulación </a> </li>");
            // XSLT [P3]
            x.println("<br/>");
            x.println("<li> <a href=\"?p=" + passwd
                    + "&pphase=21\"> Consulta 1b: ver xs alumnxs en formato de TABOA [XSLT] </a> </li>");

            // END
            x.println("<div>");
            x.println("<hr>");
            x.println("<p> Mateo G. Cid (2020/21) </p>");
            x.println("</div>");
            x.println("</body>");
            x.println("</html>");
        } else {
            x.println("<?xml version='1.0' encoding='utf-8'?><service><status>OK</status></service>");
        }
    }

    public void printCase02(PrintWriter x, ArrayList<ErrorFile> errorsList, int nWarnings, int nErrors, int nFatals) {
        // AUX
        ArrayList<ErrorFeedback> ErrorFeedbacksList;
        ArrayList<String> errorTypeList = new ArrayList<String>();
        List<String> auxErrorsList = Arrays.asList("warning", "error", "fatalerror");
        List<Integer> auxnumberErrorsList = Arrays.asList(nWarnings, nErrors, nFatals);

        if (auto == "false") {
            // INIT HTML
            x.println("<!DOCTYPE html><html>");
            x.println("<head>");
            x.println("<meta charset=\"UTF-8\" />");
            x.println("<link rel=\"shortcut icon\" href=\"./favicon.ico\">");
            x.println("<title>" + title + "</title>");
            // x.println(<link rel="stylesheet" href="p1.css">)
            x.println("</head>");

            // BODY
            x.println("<body>");
            x.println("<main>");
            x.println("<h2>" + "Servizo de consulta de expedientes académicos" + "</h2>");
            x.println("</main>");

            int z = 0;
            for (String errorType : auxErrorsList) {
                x.println("<p> Hai " + auxnumberErrorsList.get(z) + " ficheiros con " + errorType + "s: </p>");
                x.println("<ul>");
                for (int i = 0; i < errorsList.size(); i++) {
                    // get errors
                    ErrorFeedbacksList = new ArrayList<ErrorFeedback>(errorsList.get(i).getErrorsList());
                    // check if there are w/e/fe
                    errorTypeList.clear();
                    for (int k = 0; k < ErrorFeedbacksList.size(); k++) {
                        errorTypeList.add(ErrorFeedbacksList.get(k).getType());
                    }
                    // show them if thats the case
                    if (errorTypeList.contains(errorType)) {
                        x.println("<li>" + errorsList.get(i).getDocName() + "</li>");
                        x.println("<ul>");
                        for (int j = 0; j < ErrorFeedbacksList.size(); j++) {
                            if (ErrorFeedbacksList.get(j).getType().equals(errorType)) {
                                x.println("<li>" + ErrorFeedbacksList.get(j).getMessage() + "</li>");
                            }
                        }
                        x.println("</ul>");
                    }
                }
                x.println("</ul>");
                z++;
            }

            // END
            x.println("<button  onClick=\"location.href='?p=" + passwd
                    + "&pphase=01'\" type=\"button\"> Inicio </button>");
            x.println("<div>");
            x.println("<hr>");
            x.println("<p> Mateo G. Cid (2020/21) </p>");
            x.println("</div>");
            x.println("</body>");
            x.println("</html>");
        } else {
            x.println("<?xml version='1.0' encoding='utf-8' ?>");
            x.println("<wrongDocs>");
            // WARNINGS
            x.println("<warnings></warnings>");
            // ERRORS
            x.println("<errors>");
            for (int i = 0; i < errorsList.size(); i++) {
                // get errors
                ErrorFeedbacksList = new ArrayList<ErrorFeedback>(errorsList.get(i).getErrorsList());
                // check if there are w/e/fe
                errorTypeList.clear();
                for (int k = 0; k < ErrorFeedbacksList.size(); k++) {
                    errorTypeList.add(ErrorFeedbacksList.get(k).getType());
                }
                // show them if thats the case
                if (errorTypeList.contains("error")) {
                    x.println("<error>");
                    x.println("<file>" + errorsList.get(i).getDocName() + "</file>");
                    x.println("<cause>");
                    for (int j = 0; j < ErrorFeedbacksList.size(); j++) {
                        if (ErrorFeedbacksList.get(j).getType().equals("error")) {
                            x.println(ErrorFeedbacksList.get(j).getMessage() + " ");
                        }
                    }
                    x.println("</cause>");
                    x.println("</error>");
                }
            }
            x.println("</errors>");
            // FATALS
            x.println("<fatalerrors>");
            for (int i = 0; i < errorsList.size(); i++) {
                // get errors
                ErrorFeedbacksList = new ArrayList<ErrorFeedback>(errorsList.get(i).getErrorsList());
                // check if there are w/e/fe
                errorTypeList.clear();
                for (int k = 0; k < ErrorFeedbacksList.size(); k++) {
                    errorTypeList.add(ErrorFeedbacksList.get(k).getType());
                }
                // show them if thats the case
                if (errorTypeList.contains("fatalerror")) {
                    x.println("<fatalerror>");
                    x.println("<file>" + errorsList.get(i).getDocName() + "</file>");
                    x.println("<cause>");
                    for (int j = 0; j < ErrorFeedbacksList.size(); j++) {
                        if (ErrorFeedbacksList.get(j).getType().equals("fatalerror")) {
                            x.println(ErrorFeedbacksList.get(j).getMessage() + " ");
                        }
                    }
                    x.println("</cause>");
                    x.println("</fatalerror>");
                }
            }
            x.println("</fatalerrors>");
            x.println("</wrongDocs>");
        }

    }

    public void printCase11(PrintWriter x, ArrayList<String> degreeList, String auxPhase) {
        if (auto == "false") {
            // INIT HTML
            x.println("<!DOCTYPE html><html>");
            x.println("<head>");
            x.println("<meta charset=\"UTF-8\" />");
            x.println("<link rel=\"shortcut icon\" href=\"./favicon.ico\">");
            x.println("<title>" + title + "</title>");
            // x.println(<link rel="stylesheet" href="p1.css">)
            x.println("</head>");

            // BODY
            x.println("<body>");
            x.println("<main>");
            x.println("<h2>" + "Servizo de consulta de expedientes académicos" + "</h2>");
            x.println("</main>");

            // PPHASE
            x.println("<p>" + "Consulta 1: Fase 1" + "</p>");
            x.println("<p>" + "Selecciona unha titulación:" + "</p>");
            x.println("<ol>");
            for (int i = 0; i < degreeList.size(); i++) {
                String degreeName = degreeList.get(i);
                x.println("<li> <a href=\"?p=" + passwd + "&pphase="+ auxPhase +"&pdegree=" + degreeName + "\">" + degreeName
                        + "</a> </li>");
            }
            x.println("</ol>");

            // END
            x.println("<div>");
            x.println("<button  onClick=\"location.href='?p=" + passwd
                    + "&pphase=01'\" type=\"button\"> Inicio </button>");
            x.println("<hr>");
            x.println("<p> Mateo G. Cid (2020/21) </p>");
            x.println("</div>");
            x.println("</body>");
            x.println("</html>");
        } else {
            x.println("<?xml version='1.0' encoding='utf-8' ?>");
            x.println("<degrees>");
            for (int i = 0; i < degreeList.size(); i++) {
                x.println("<degree>" + degreeList.get(i) + "</degree>");
            }
            x.println("</degrees>");
        }
    }

    public void printCase12(PrintWriter x, ArrayList<Subject> subjectsList, String degree) {
        if (auto == "false") {
            // INIT HTML
            x.println("<!DOCTYPE html><html>");
            x.println("<head>");
            x.println("<meta charset=\"UTF-8\" />");
            x.println("<link rel=\"shortcut icon\" href=\"./favicon.ico\">");
            x.println("<title>" + title + "</title>");
            // x.println(<link rel="stylesheet" href="p1.css">)
            x.println("</head>");

            // BODY
            x.println("<body>");
            x.println("<main>");
            x.println("<h2>" + "Servizo de consulta de expedientes académicos" + "</h2>");
            x.println("</main>");

            // PPHASE
            x.println("<p>" + "Consulta 1: Fase 2 (Titulación: " + degree + ")</p>");
            x.println("<p>" + "Selecciona unha asignatura" + "</p>");
            x.println("<ol>");
            for (int i = 0; i < subjectsList.size(); i++) {
                String subjectName = subjectsList.get(i).getName();
                x.println("<li> <a href=\"?p=" + passwd + "&pphase=13&pdegree=" + degree + "&psubject=" + subjectName
                        + "\">" + "Nome: " + subjectName + " -- " + "Curso = " + subjectsList.get(i).getCourse()
                        + " -- " + "Tipo = " + subjectsList.get(i).getType() + "</a> </li>");
            }
            // END
            x.println("<div>");
            x.println("<button onClick=\"location.href='?p=" + passwd
                    + "&pphase=01'\" type=\"button\"> Inicio </button>");
            x.println(
                    "<button onClick=\"location.href='?p=" + passwd + "&pphase=11'\" type=\"button\"> Atrás </button>");
            x.println("<hr>");
            x.println("<p> Mateo G. Cid (2020/21) </p>");
            x.println("</div>");
            x.println("</body>");
            x.println("</html>");
        } else {
            x.println("<?xml version='1.0' encoding='utf-8' ?>");
            x.println("<subjects>");
            for (int i = 0; i < subjectsList.size(); i++) {
                x.println("<subject course='" + subjectsList.get(i).getCourse() + "' type='"
                        + subjectsList.get(i).getType() + "'>" + subjectsList.get(i).getName() + "</subject>");
            }
            x.println("</subjects>");
        }
    }

    public void printCase13(PrintWriter x, ArrayList<Student> studentsList, String degree, String subject) {
        if (auto == "false") {
            // INIT HTML
            x.println("<!DOCTYPE html><html>");
            x.println("<head>");
            x.println("<meta charset=\"UTF-8\" />");
            x.println("<link rel=\"shortcut icon\" href=\"./favicon.ico\">");
            x.println("<title>" + title + "</title>");
            // x.println(<link rel="stylesheet" href="p1.css">)
            x.println("</head>");

            // BODY
            x.println("<body>");
            x.println("<main>");
            x.println("<h2>" + "Servizo de consulta de expedientes académicos" + "</h2>");
            x.println("</main>");

            // PPHASE
            x.println("<p>" + "Consulta 1: Fase 3 (Titulación: " + degree + ", Asignatura: " + subject + ")</p>");
            x.println("<p>" + "Selecciona un(a) alumnx" + "</p>");
            x.println("<ol>");
            for (int i = 0; i < studentsList.size(); i++) {
                x.println("<li>" + "Nome: " + studentsList.get(i).getName() + " -- " + "ID = "
                        + studentsList.get(i).getID() + " -- " + "Dirección = " + studentsList.get(i).getAddress()
                        + "</li>");
            }
            x.println("</ol>");

            // END
            x.println("<div>");
            x.println("<button onClick=\"location.href='?p=" + passwd
                    + "&pphase=01'\" type=\"button\"> Inicio </button>");
            x.println("<button onClick=\"location.href='?p=" + passwd + "&pphase=12&pdegree=" + degree
                    + "'\" type=\"button\"> Atrás </button>");
            x.println("<hr>");
            x.println("<p> Mateo G. Cid (2020/21) </p>");
            x.println("</div>");
            x.println("</body>");
            x.println("</html>");
        } else {
            x.println("<?xml version='1.0' encoding='utf-8' ?>");
            x.println("<students>");
            for (int i = 0; i < studentsList.size(); i++) {
                x.println("<student id=\'" + studentsList.get(i).getID() + "\' address=\'"
                        + studentsList.get(i).getAddress() + "\'>" + studentsList.get(i).getName() + "</student>");
            }
            x.println("</students>");
        }
    }

    /* ERROR SCREENS */
    public void printWrongPasswd(PrintWriter x, boolean triedPassword) {
        if (auto == "false") {
            // INIT HTML
            x.println("<!DOCTYPE html><html>");
            x.println("<head>");
            x.println("<meta charset=\"UTF-8\" />");
            x.println("<link rel=\"shortcut icon\" href=\"./favicon.ico\">");
            x.println("<title>" + title + "</title>");
            // x.println(<link rel="stylesheet" href="p1.css">)
            x.println("</head>");

            // BODY
            x.println("<body>");
            x.println("<main>");
            if (triedPassword) {
                x.println("<h2>" + "Bo intento....contrasinal incorrecto!" + "</h2>");
            } else {
                x.println("<h2>" + "Hai que introducir unha contrasinal" + "</h2>");
            }
            x.println("</main>");

            // END
            x.println("<div>");
            x.println("<hr>");
            x.println("<p> Mateo G. Cid (2020/21) </p>");
            x.println("</div>");
            x.println("</body>");
            x.println("</html>");
        } else {
            x.println("<?xml version='1.0' encoding='utf-8'?>");
            if (triedPassword) {
                x.println("<wrongRequest>bad passwd</wrongRequest>");
            } else {
                x.println("<wrongRequest>no passwd</wrongRequest>");
            }
        }
    }

    public void printMissingParameters(PrintWriter x, String missingP) {
        if (auto == "false") {
            // INIT HTML
            x.println("<!DOCTYPE html><html>");
            x.println("<head>");
            x.println("<meta charset=\"UTF-8\" />");
            x.println("<link rel=\"shortcut icon\" href=\"./favicon.ico\">");
            x.println("<title>" + title + "</title>");
            // x.println(<link rel="stylesheet" href="p1.css">)
            x.println("</head>");

            // BODY
            x.println("<body>");
            x.println("<main>");
            x.println("<h2>" + "Fáltache o parámetro: " + missingP + "</h2>");

            x.println("</main>");

            // END
            x.println("<div>");
            x.println("<hr>");
            x.println("<p> Mateo G. Cid (2020/21) </p>");
            x.println("</div>");
            x.println("</body>");
            x.println("</html>");
        } else {
            x.println("<?xml version='1.0' encoding='utf-8'?>");
            x.println("<wrongRequest>no param:" + missingP + "</wrongRequest>");

        }
    }

    // GETTERS & SETTERS
    public String getAuto() {
        return this.auto;
    }

    public void setAuto(String newAuto) {
        this.auto = newAuto;
    }
}
