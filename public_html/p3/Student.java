package p3;

public class Student {
    String name;
    String id;
    String address;
    boolean hasDni;

    // CONSTRUCTORS
    public Student(String name, String address, String id, boolean hasDni) {
        this.name = name;
        this.address = address;
        this.id = id;
        this.hasDni = hasDni;
    }

    // GETTERS & SETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean hasDni() {
        return hasDni;
    }

    public void setHasDni(boolean hasDni) {
        this.hasDni = hasDni;
    }

    /* AUX */
    public boolean sortDni() {
        return !hasDni;
    }
}
