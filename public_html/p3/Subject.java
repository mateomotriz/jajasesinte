package p3;

public class Subject {
    String name;
    String course;
    String type;

    // CONSTRUCTORS
    public Subject(String name, String course, String type) {
        this.name = name;
        this.course = course;
        this.type = type;
    }

    public Subject(String name, String course) {
        this(name, course, "");
    }

    // GETTERS & SETTERS
    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
