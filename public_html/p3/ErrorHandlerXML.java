package p3;

import java.util.ArrayList;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXParseException;

public class ErrorHandlerXML extends DefaultHandler {
   int errorHandler = 0;
   int nWarnings, nErrors, nFatalErrors;
   ArrayList<ErrorFeedback> errorsList;

   public ErrorHandlerXML() {
      nWarnings = 0;
      nErrors = 0;
      nFatalErrors = 0;
      errorsList = new ArrayList<ErrorFeedback>();
   }

   public void warning(SAXParseException spe) {
      ErrorFeedback warning = new ErrorFeedback(spe.toString().trim(), "warning");
      errorsList.add(warning);
      errorHandler = 1;
      nWarnings++;
      // System.out.println("Warning: " + spe.toString());
   }

   public void error(SAXParseException spe) {
      ErrorFeedback error = new ErrorFeedback(spe.toString(), "error");
      errorsList.add(error);
      errorHandler = 2;
      nErrors++;
      // System.out.println("Error: " + spe.toString());
   }

   public void fatalerror(SAXParseException spe) {
      ErrorFeedback fatalerror = new ErrorFeedback(spe.toString(), "fatalerror");
      errorsList.add(fatalerror);
      errorHandler = 3;
      nFatalErrors++;
      // System.out.println("Fatal Error: " + spe.toString());
   }

   // GETTER(s)
   public int getHandlerError() {
      int prevError = errorHandler;
      errorHandler = 0;
      return prevError;
   }

   public ArrayList<ErrorFeedback> getErrorFeedbacks() {
      ArrayList<ErrorFeedback> prevList = new ArrayList<ErrorFeedback>(errorsList);
      errorsList.clear();
      return prevList;
   }

   // REMOVE HEADER
   public String removeHeader(String s) {
      return s.replace("org.xml.sax.SAXParseException; systemId:", "").trim();
   }
}
