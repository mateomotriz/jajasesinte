package p3;

import java.util.*;
// XPATH
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;
// XSLT
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.*;
// OTHERS
import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class Mateo {
    // SCHEMA
    final String INIT_FILE = "http://gssi.det.uvigo.es/users/agil/public_html/SINT/20-21/teleco.xml";
    // final String INIT_FILE = "http://localhost:7033/sint33/p2/teleco.xml";
    static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
    static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
    static ErrorHandlerXML errorHandler;

    public static void main(String argv[]) {
        parseXML();
    }

    static void parseXML() {
        try {
            // FILES
            //String pathSchema = getServletContext().getRealPath("/p2/eaml.xsd");
            File schemaFile = new File("webapps/p3/eaml.xsd");
            File dataFile = new File("webapps/p3/teleco.xml");
            File xslFile = new File("webapps/p3/eaml-html.xslt");

            // CONFIGURE SCHEMA
            DocumentBuilder db = null;
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setValidating(true);
            dbf.setNamespaceAware(true);

            // SCHEMA
            dbf.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            dbf.setAttribute(JAXP_SCHEMA_SOURCE, schemaFile);

            // CONFIGURE ERROR HANDLER
            errorHandler = new ErrorHandlerXML();
            db = dbf.newDocumentBuilder();
            db.setErrorHandler(errorHandler);

            // PARSE
            doc = db.parse(dataFile);

            // TRANSFORMATION
            TransformerFactory factory = TransformerFactory.newInstance();
            Source xslSrc = new StreamSource(xslFile);
            Transformer transformer = factory.newTransformer(xslSrc);

            DOMSource src = new DOMSource(doc);
            transformer.transform(src, new StreamResult(new File("mateo.html")));

        } catch (Exception ex) {
            System.out.println("problem w smth:  " + ex.toString());
        }
    }
}
