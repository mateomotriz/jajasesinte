package p3;

public class ErrorFeedback {
    String message; 
    String type;

    public ErrorFeedback(String message, String type) {
        this.message = message;
        this.type = type;
    }

    // GETTERS & SETTERS
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
