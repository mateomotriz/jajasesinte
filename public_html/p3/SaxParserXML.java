package p3;

import java.util.ArrayList;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

public class SaxParserXML extends DefaultHandler {
    // AUX
    private StringBuilder sb = new StringBuilder();
    private StringBuilder sbAux = new StringBuilder();
    private String rootElement = "", currentElement = "";
    private String psubject = "";
    private boolean matchSubject = false;

    // VARIABLES
    String rootDegree;
    ArrayList<String> eamlList = new ArrayList<>();

    // SUBJECTS
    ArrayList<Subject> subjectsList = new ArrayList<>();
    String subjectName = "", subjectCourse = "", subjectType = "";

    // STUDENTS
    ArrayList<Student> studentsList = new ArrayList<>();
    String studentName = "", studentID = "", studentAddress = "";
    boolean hasDni = false;

    // SM
    String state = "OTHER";

    // EMPTY CONSTRUCTOR
    public SaxParserXML() {
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentElement = qName;
        switch (qName) {
            case "Degree":
            case "Course":
            case "Subject":
            case "Student":
                rootElement = qName;
                break;
        }

        // GET ATTRIBUTES SUBJECT
        if (state.equals("SUBJECT")) {
            switch (currentElement) {
                case "Course":
                    subjectCourse = attributes.getValue(0);
                    break;
                case "Subject":
                    for (int i = 0; i < attributes.getLength(); i++) {
                        if (attributes.getQName(i).equals("type")) {
                            subjectType = attributes.getValue(i);
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        switch (state) {
            case "EAML":
                // ROOT DEGREE
                switch (rootElement) {
                    case "Degree":
                        if (currentElement.equals("Name")) {
                            sb.append(ch, start, length);
                        }
                        break;
                }
                // GET EAMLS
                if (currentElement.equals("EAML")) {
                    sb.append(ch, start, length);
                }
                break;
            case "SUBJECT":
                switch (rootElement) {
                    case "Subject":
                        if (currentElement.equals("Name")) {
                            sb.append(ch, start, length);
                        }
                        break;
                }
                break;
            case "STUDENT":
                switch (rootElement) {
                    case "Subject":
                        if (currentElement.equals("Name")) {
                            sb.append(ch, start, length);
                        }
                        break;
                    case "Student":
                        if (matchSubject) {
                            if (currentElement.equals("Name") || currentElement.equals("Resident")
                                    || currentElement.equals("Dni")) {
                                sb.append(ch, start, length);
                            } else if (currentElement.equals("Grade") || currentElement.equals("EAML")) {
                                // nop
                            } else {
                                sbAux.append(ch, start, length);
                            }
                        }
                        break;
                }
                break;
            case "OTHER":
            default:
                // notting
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (state) {
            case "EAML":
                // ROOT DEGREE
                switch (rootElement) {
                    case "Degree":
                        if (qName.equals("Name")) {
                            rootDegree = char2String();
                        }
                        break;
                }
                // GET EAMLS
                if (qName.equals("EAML")) {
                    eamlList.add(char2String());
                }
                break;
            case "SUBJECT":
                switch (rootElement) {
                    // SUBJECT NAME
                    case "Subject":
                        if (qName.equals("Name")) {
                            subjectName = char2String();
                        }
                        break;
                }
                // CLOSE SUBJECT
                if (qName.equals("Subject")) {
                    Subject subj = new Subject(subjectName.trim(), subjectCourse.trim(), subjectType.trim());
                    // System.out.println("SUBJECT: " + subj.getName());
                    subjectsList.add(subj);
                }
                break;
            case "STUDENT":
                switch (rootElement) {
                    // SUBJECT NAME
                    case "Subject":
                        if (qName.equals("Name")) {
                            if (char2String().trim().equals(psubject.trim())) {
                                matchSubject = true;
                            }
                        }
                        break;
                    case "Student":
                        if (matchSubject) {
                            if (qName.equals("Name")) {
                                studentName = char2String();
                            } else if (qName.equals("Resident")) {
                                hasDni = false;
                                studentID = char2String();
                            } else if (qName.equals("Dni")) {
                                hasDni = true;
                                studentID = char2String();
                            } else if (qName.equals("Student")) {
                                studentAddress = sbAux.toString();
                                sbAux.delete(0, sbAux.length());
                            }
                        }
                        break;
                }
                // CLOSE SUBJECT
                if (matchSubject) {
                    if (qName.equals("Subject")) {
                        matchSubject = false;
                        state = "OTHER";
                    } else if (qName.equals("Student")) {
                        Student stu = new Student(studentName.trim(), studentAddress.trim(), studentID.trim(), hasDni);
                        studentsList.add(stu);
                    }
                }
                break;
            case "OTHER":
            default:
                // notting
                break;
        }

        currentElement = "";
    }

    /* GETTERS & SETTERS */
    public String getRootDegree() {
        return rootDegree;
    }

    public ArrayList<String> getC1Eamls() {
        ArrayList<String> prevList = new ArrayList<String>(eamlList);
        eamlList.clear();
        return prevList;
    }

    public ArrayList<Subject> getC1Subjects() {
        ArrayList<Subject> prevList = new ArrayList<Subject>(subjectsList);
        subjectsList.clear();
        return prevList;
    }

    public ArrayList<Student> getC1Students() {
        ArrayList<Student> prevList = new ArrayList<Student>(studentsList);
        studentsList.clear();
        return prevList;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setSubject(String psubject) {
        this.psubject = psubject;
    }

    public String getSubject() {
        return psubject;
    }

    /* AUX */
    private String char2String() {
        String out = sb.toString();
        sb.delete(0, sb.length());
        return out.trim();
    }
}
