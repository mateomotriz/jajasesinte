package p2;

// Mateo G. Cid :)
// UTILS
import java.util.*;

// SERVLET
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// XPATH
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;

// OTHERS
import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class Sint33P2 extends HttpServlet {
      // DATA 2 RETRIEVE
      private final String SUPER_SECRET_PASSWORD = "MAT30KRACK";

      // DEGREE'S DATA
      HashMap<String, Document> mapDocs = new HashMap<>();
      ArrayList<String> unreadDocs = new ArrayList<>();
      ArrayList<String> filesList = new ArrayList<String>();
      ArrayList<ErrorFile> errorsList = new ArrayList<ErrorFile>();
      ArrayList<Subject> subjectsList = new ArrayList<>();
      ArrayList<Student> studentsList = new ArrayList<>();

      // ERROR CODES
      private final String ERROR_NOPASSWORD = "NO PASSWD";
      private final String ERROR_WRONGPASSWD = "WRONG PASSWD";
      private final String ERROR_PARAMETERS = "MISSING PARAMETERS";
      private final String NO_ERROR = "OK";

      // SCHEMA
      final String INIT_FILE = "http://gssi.det.uvigo.es/users/agil/public_html/SINT/20-21/teleco.xml";
      // final String INIT_FILE = "http://localhost:7033/sint33/p2/teleco.xml";
      final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
      final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
      final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
      ErrorHandlerXML errorHandler;

      // XPATH
      XPathFactory xpathFactory;
      XPath xpath;

      // STATE MACHINE
      enum FileState {
            INIT, VALIDATE, SAVE, LOOKFOR, ERROR_HANDLER, FINISH
      }

      /* MAIN */
      @Override
      public void init() throws ServletException {
            // XML
            try {
                  xmlHandler();
            } catch (ParserConfigurationException pce) {
                  System.out.println("ParserConfigurationException: " + pce.getMessage());
            }

            super.init();
      }

      /* SERVLET */
      @Override
      public void doGet(HttpServletRequest request, HttpServletResponse response) {
            // GET SYSTEM PARAMETERS
            getParametersURL(request, response);
      }

      /* METHODS ************************** :) */

      /* PARAMETERS FROM URL */
      private void getParametersURL(HttpServletRequest request, HttpServletResponse response) {
            // initially
            String auto = "", pphase = "", pdegree = "", psubject = "", passwd = "";
            String errorCode = "", missingParameter = "";
            errorCode = NO_ERROR;

            // aux
            boolean auxPasswd = true;

            try {
                  // check passwd
                  passwd = request.getParameter("p");
                  if (passwd == null) {
                        errorCode = ERROR_NOPASSWORD;
                        passwd = "";
                        auxPasswd = false;
                  } else if ((!passwd.equals(SUPER_SECRET_PASSWORD))) {
                        errorCode = ERROR_WRONGPASSWD;
                        auxPasswd = false;
                  }

                  // only if the passwd is correct!
                  if (auxPasswd) {
                        // auto mode
                        auto = request.getParameter("auto");
                        if (auto == null) {
                              auto = "false";
                        } else if (!auto.equals("true")) {
                              auto = "false";
                        }

                        // which phase
                        pphase = request.getParameter("pphase");
                        if (pphase == null) {
                              pphase = "01";
                        }

                        // which degree
                        pdegree = request.getParameter("pdegree");
                        if (pdegree == null) {
                              if (pphase.equals("12") || pphase.equals("13")) {
                                    missingParameter = "pdegree";
                                    errorCode = ERROR_PARAMETERS;
                              }
                        }

                        // which subject
                        psubject = request.getParameter("psubject");
                        if (psubject == null) {
                              if (pphase.equals("13")) {
                                    errorCode = ERROR_PARAMETERS;
                                    if (pdegree == null) {
                                          missingParameter = "pdegree";
                                    } else {
                                          missingParameter = "psubject";
                                    }
                              }
                        }
                  }

                  // INIT FRONT END
                  FrontEnd frontEnd = new FrontEnd(auto, passwd);

                  // INITIAL SETUP & PRINT
                  if (auto == "false") {
                        response.setContentType("text/html");
                  } else if (auto == "true") {
                        response.setContentType("text/xml");
                  }
                  response.setCharacterEncoding("UTF-8");
                  PrintWriter x = response.getWriter();

                  if (errorCode.equals(NO_ERROR)) {
                        switch (pphase) {
                              case "01":
                                    frontEnd.printCase01(x);
                                    break;
                              case "02":
                                    frontEnd.printCase02(x, errorsList, countErrors("warning"), countErrors("error"),
                                                countErrors("fatalerror"));
                                    break;
                              case "11":
                                    frontEnd.printCase11(x, getC1Degrees());
                                    break;
                              case "12":
                                    frontEnd.printCase12(x, getC1Subjects(pdegree), pdegree);
                                    break;
                              case "13":
                                    frontEnd.printCase13(x, getC1Students(pdegree, psubject), pdegree, psubject);
                                    break;
                              default:
                                    frontEnd.printCase01(x);
                                    break;
                        }
                  } else {
                        switch (errorCode) {
                              // NO PASSWORD
                              case ERROR_NOPASSWORD:
                                    frontEnd.printWrongPasswd(x, false);
                                    break;
                              // WRONG PASSWD
                              case ERROR_WRONGPASSWD:
                                    frontEnd.printWrongPasswd(x, true);
                                    break;
                              // MISSING PARAMETERS
                              case ERROR_PARAMETERS:
                                    frontEnd.printMissingParameters(x, missingParameter);
                                    break;

                              default:
                                    break;
                        }
                  }
            }

            catch (Exception ex) {
                  System.out.println("Algo foi mal:" + ex.toString());
            }
      }

      /* XML PARSER */
      private void xmlHandler() throws ParserConfigurationException {
            // AUX
            FileState fileState = FileState.INIT;
            boolean readFiles = false;

            // CONFIGURE SCHEMA
            String currentDoc;
            DocumentBuilder db = null;
            Document doc = null;
            DocumentBuilderFactory dbf;
            dbf = DocumentBuilderFactory.newInstance();
            dbf.setValidating(true);
            dbf.setNamespaceAware(true);

            // SCHEMA
            dbf.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            String pathSchema = getServletContext().getRealPath("/p2/eaml.xsd");
            File schemaFile = new File(pathSchema);
            dbf.setAttribute(JAXP_SCHEMA_SOURCE, schemaFile);

            // CONFIGURE ERROR HANDLER
            db = null;
            errorHandler = new ErrorHandlerXML();
            db = dbf.newDocumentBuilder();
            db.setErrorHandler(errorHandler);

            // XPATH
            NodeList eamlNodeList;
            NodeList degreeNodeList;
            xpathFactory = XPathFactory.newInstance();
            xpath = xpathFactory.newXPath();

            // URL
            String[] absoluteURL;
            String fileURL = "";
            String prefixURL = "";

            while (!readFiles) {
                  switch (fileState) {
                        case INIT:
                              unreadDocs.clear();
                              unreadDocs.add(INIT_FILE);
                              fileState = FileState.VALIDATE;
                              break;
                        case VALIDATE:
                              // valid document?
                              currentDoc = unreadDocs.get(0);
                              try {
                                    doc = db.parse(currentDoc);

                                    // get URL (to fix the relative paths)
                                    absoluteURL = currentDoc.split("/");
                                    fileURL = absoluteURL[absoluteURL.length - 1];
                                    prefixURL = currentDoc.replace(fileURL, "");

                                    // check if there were any errors
                                    if (errorHandler.getHandlerError() != 0) {
                                          // get errors
                                          ErrorFile errorFile = new ErrorFile(currentDoc,
                                                      errorHandler.getErrorFeedbacks());

                                          // finish doc
                                          System.out.println("MAL: " + currentDoc + " has this errors:"
                                                      + errorFile.getErrorsList().size());
                                          errorsList.add(errorFile);
                                          fileState = FileState.FINISH;
                                    } else {
                                          // null doc?
                                          if (doc == null) {
                                                System.out.println("NULL: DOC - " + currentDoc);
                                                fileState = FileState.FINISH;
                                          } else {
                                                System.out.println("OK: DOC - " + currentDoc);
                                                fileState = FileState.SAVE;
                                          }
                                    }
                              } catch (IOException ioe) {
                                    ErrorFeedback ioeMessage = new ErrorFeedback(ioe.getMessage().trim(), "fatalerror");
                                    errorsList.add(new ErrorFile(currentDoc, ioeMessage));
                                    fileState = FileState.FINISH;
                              } catch (SAXException se) {
                                    String auxSax = se.getMessage().trim();
                                    auxSax = auxSax.replace("\"<", "");
                                    auxSax = auxSax.replace(">\"", "");
                                    auxSax = auxSax.replace("/", "");
                                    ErrorFeedback seMessage = new ErrorFeedback(auxSax, "fatalerror");
                                    errorsList.add(new ErrorFile(currentDoc, seMessage));
                                    fileState = FileState.FINISH;
                              }

                              // mark document as read (doesnt matter if its not ok)
                              unreadDocs.remove(0);
                              break;
                        case SAVE:
                              // check if it already exists
                              try {
                                    String xpathDegree = "Degree/Name/text()";
                                    degreeNodeList = (NodeList) xpath.evaluate(xpathDegree, doc,
                                                XPathConstants.NODESET);
                                    String rootDegree = degreeNodeList.item(0).getNodeValue();
                                    if (!mapDocs.containsKey(rootDegree)) {
                                          mapDocs.put(rootDegree, doc);
                                          fileState = FileState.LOOKFOR;
                                    } else {
                                          fileState = FileState.FINISH;
                                    }
                              } catch (XPathExpressionException xpe) {
                                    System.out.println("XPathException: " + xpe.getMessage());
                                    fileState = FileState.FINISH;
                              }

                              break;
                        case LOOKFOR:
                              // look for more files
                              try {
                                    String xpathEAMLS = "Degree/Course/Subject/Student/EAML/text()";
                                    // String xpathEAMLS = "//EAML/text()";
                                    eamlNodeList = (NodeList) xpath.evaluate(xpathEAMLS, doc, XPathConstants.NODESET);
                                    for (int i = 0; i < eamlNodeList.getLength(); i++) {
                                          String eaml = (String) eamlNodeList.item(i).getNodeValue();
                                          // check link (abs/rel)
                                          if (!eaml.startsWith("http:/")) {
                                                eaml = prefixURL + eaml;
                                          }
                                          unreadDocs.add(eaml);
                                    }
                              } catch (XPathExpressionException xpe) {
                                    System.out.println("XPathException: " + xpe.getMessage());
                                    fileState = FileState.FINISH;
                              }

                              fileState = FileState.FINISH;
                        case FINISH:
                              if (unreadDocs.isEmpty()) {
                                    // sort ABC
                                    Collections.sort(errorsList, Comparator.comparing(ErrorFile::getDocName));

                                    // init servlet
                                    readFiles = true;
                              } else {
                                    fileState = FileState.VALIDATE;
                              }
                              break;
                        case ERROR_HANDLER:
                              break;
                        default:
                              break;
                  }
            }
      }

      /* METHODS! */
      private ArrayList<String> getC1Degrees() {
            filesList.clear();
            for (String degree : mapDocs.keySet()) {
                  filesList.add(degree);
            }

            // sort ABC
            Collections.sort(filesList, String.CASE_INSENSITIVE_ORDER);
            return filesList;
      }

      private ArrayList<Subject> getC1Subjects(String degree) {
            // SUBJECTS
            Document doc = mapDocs.get(degree);
            String xpathCourse = "//Course";

            try {
                  subjectsList.clear();
                  NodeList coursesNodeList = (NodeList) xpath.evaluate(xpathCourse, doc, XPathConstants.NODESET);
                  for (int i = 0; i < coursesNodeList.getLength(); i++) {
                        // attr number
                        String course = (String) coursesNodeList.item(i).getAttributes().item(0).getNodeValue();

                        // look 4 subjects
                        String xpathSubject = "./Subject";
                        NodeList subjectsNodeList = (NodeList) xpath.evaluate(xpathSubject, coursesNodeList.item(i),
                                    XPathConstants.NODESET);
                        for (int j = 0; j < subjectsNodeList.getLength(); j++) {
                              Node subjectNode = subjectsNodeList.item(j);
                              // add subjects
                              String subjectName = (String) xpath.evaluate("./Name/text()", subjectNode,
                                          XPathConstants.STRING);
                              String subjectType = (String) xpath.evaluate("./@type", subjectNode,
                                          XPathConstants.STRING);
                              Subject subj = new Subject(subjectName.trim(), course.trim(), subjectType.trim());
                              subjectsList.add(subj);
                        }
                  }
            } catch (XPathExpressionException xpee) {
                  System.out.println("Xpath exception: " + xpee.getMessage());
                  System.exit(-1);
            }

            // sort ABC
            Collections.sort(subjectsList, Comparator.comparing(Subject::getName));
            Collections.sort(subjectsList, Comparator.comparing(Subject::getCourse));
            return subjectsList;
      }

      private ArrayList<Student> getC1Students(String degree, String subject) {
            // STUDENTS
            Document doc = mapDocs.get(degree);
            String xpathStudents = "//Subject[Name='" + subject + "']/Student";
            boolean hasDni = true;

            try {
                  studentsList.clear();
                  NodeList studentsNodeList = (NodeList) xpath.evaluate(xpathStudents, doc, XPathConstants.NODESET);
                  for (int i = 0; i < studentsNodeList.getLength(); i++) {
                        Node studentNode = studentsNodeList.item(i);

                        // look4
                        String studentName = (String) xpath.evaluate("./Name/text()", studentNode,
                                    XPathConstants.STRING);

                        String studentAddress = "";
                        NodeList addrNodeList = (NodeList) xpath.evaluate("./s()", studentNode,
                                    XPathConstants.NODESET);
                        for (int z = 0; z < addrNodeList.getLength(); z++) {
                              if (!addrNodeList.item(z).getNodeValue().trim().equals("")) {
                                    studentAddress = addrNodeList.item(z).getNodeValue().trim();
                              }
                        }

                        // check dni/residentID
                        hasDni = true;
                        String studentId = (String) xpath.evaluate("./Dni/text()", studentNode, XPathConstants.STRING);
                        if (studentId == null || studentId.isEmpty()) {
                              hasDni = false;
                              studentId = (String) xpath.evaluate("./Resident/text()", studentNode,
                                          XPathConstants.STRING);
                        }

                        // create student
                        Student stu = new Student(studentName.trim(), studentAddress.trim(), studentId.trim(), hasDni);
                        studentsList.add(stu);
                  }
            } catch (XPathExpressionException xpee) {
                  System.out.println("Xpath exception: " + xpee.getMessage());
                  System.exit(-1);
            }

            // sort ABC
            Collections.sort(studentsList, Comparator.comparing(Student::getName));
            Collections.sort(studentsList, Comparator.comparing(Student::sortDni));

            return studentsList;
      }

      /*
       * private ArrayList<ErrorFile> getC1Errors() { return
       * errorHandler.getErrorsList(); }
       */

      /* AUX */
      public int countErrors(String errorType) {
            int errorCounter = 0;
            ArrayList<String> errorTypeList = new ArrayList<String>();

            for (int i = 0; i < errorsList.size(); i++) {
                  // get errors
                  ArrayList<ErrorFeedback> efList = new ArrayList<ErrorFeedback>(errorsList.get(i).getErrorsList());
                  // check if there are w/e/fe
                  errorTypeList.clear();
                  for (int k = 0; k < efList.size(); k++) {
                        errorTypeList.add(efList.get(k).getType());
                  }
                  // show them if thats the case
                  if (errorTypeList.contains(errorType)) {
                        errorCounter++;
                  }
            }
            return errorCounter;
      }
}
