package p2;
import java.util.ArrayList;

public class ErrorFile {
    String document;
    ArrayList<ErrorFeedback> errorsList = new ArrayList<ErrorFeedback>();
    ErrorFeedback ErrorFeedback;

    // MULTIPLE CONSTRUCTORS
    public ErrorFile(String doc, ArrayList<ErrorFeedback> eList, ErrorFeedback em) {
        this.document = doc;
        this.errorsList = eList;
        this.ErrorFeedback = em;
    }

    public ErrorFile(String doc, ArrayList<ErrorFeedback> eList) {
        this(doc, eList, null);
    }

    public ErrorFile(String doc, ErrorFeedback em) {
        this(doc, new ArrayList<ErrorFeedback>(), em);
        // init array
        errorsList.add(em);
    }

    // GETTERS & SETTERS
    public String getDocName() {
        return this.document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public ArrayList<ErrorFeedback> getErrorsList() {
        return errorsList;
    }

    public void setErrorsList(ArrayList<ErrorFeedback> errorsList) {
        this.errorsList = errorsList;
    }
}
