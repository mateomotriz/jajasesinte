import java.io.IOException;
import java.io.PrintWriter;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class HelloWorldExample extends HttpServlet {
    Logger logger = LogManager.getLogger(HelloWorldExample.class.getName());

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
    {  
		try{
            logger.info("alaverga");

            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();

            out.println("<!DOCTYPE html><html>");
            out.println("<head>");
            out.println("<meta charset=\"UTF-8\" />");

            String user = request.getParameter("user");
            String title = "Welcome to the jungle";

            out.println("<title>" + title + "</title>");
            out.println("</head>");
            out.println("<body bgcolor=\"white\">");
            out.println("<p> user: " +  user + " </p>");
            out.println("</body>");
            out.println("</html>");
		}
		catch (Exception ex){
            System.out.println("Algo fue mal:"+ex.toString());}
    }
}



